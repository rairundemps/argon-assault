using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ColliderHandler : MonoBehaviour
{
    private PlayerShip _playerShipControls;
    private ParticleSystem _explosion;
    private Rigidbody _rigidbody;

    private void Start()
    {
        _playerShipControls = GetComponent<PlayerShip>();
        _explosion = GetComponent<ParticleSystem>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log($"Object {gameObject.name} collided with {collision.gameObject.name}");
    }

    private void OnTriggerEnter(Collider other)
    {
        _playerShipControls.enabled = false;
        _explosion.Play();
        _rigidbody.isKinematic = false;
        StartCoroutine(nameof(ReloadSceneWithDelay));
    }

    private IEnumerator ReloadSceneWithDelay()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
