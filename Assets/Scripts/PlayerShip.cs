using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : MonoBehaviour
{
    [Header("General Setup Settings")]
    [Tooltip("How fast ship moves based upon player input")]
    [SerializeField] private float _speed = 20f;
    [Tooltip("The upper limit of the horizontal movement of the ship")]
    [SerializeField] private float _xMoveRange = 15f;
    [Tooltip("The upper limit of the vertical movement of the ship")]
    [SerializeField] private float _yMoveRange = 12f;

    [Header("Screen position based tuning")]
    [SerializeField] private float _positionPitchFactor = -1f;
    [SerializeField] private float _positionYawFactor = 2.5f;
    
    [Header("Player input based tuning")]
    [SerializeField] private float _controlRollFactor = -45f;
    [SerializeField] private float _controlPitchFactor = -10f;

    [Header("Laser gun array")]
    [Tooltip("Add all player lasers here")]
    [SerializeField] private ParticleSystem[] _lasers;

    private void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        bool shootInput = Input.GetButton("Fire1");

        Move(horizontalInput, verticalInput);
        Rotate(horizontalInput, verticalInput);
        Shoot(shootInput);
    }

    private void Move(float horizontalInput, float verticalInput) {
        float OffsetX = horizontalInput * _speed * Time.deltaTime;
        float positionX = transform.localPosition.x + OffsetX;
        float clampedPositionX = Mathf.Clamp(positionX, -_xMoveRange, _xMoveRange);

        float OffsetY = verticalInput * _speed * Time.deltaTime;
        float positionY = transform.localPosition.y + OffsetY;
        float clampedPositionY = Mathf.Clamp(positionY, -_yMoveRange, _yMoveRange);

        transform.localPosition = new Vector3(clampedPositionX, clampedPositionY, transform.localPosition.z);
    }

    private void Rotate(float horizontalInput, float verticalInput) {
        float pitchDueToPosition = transform.localPosition.y * _positionPitchFactor;
        float pitchDueToControl = verticalInput *_controlPitchFactor;
        float pitch = pitchDueToPosition + pitchDueToControl;

        float yaw = transform.localPosition.x * _positionYawFactor;
        float rollDueToControl = horizontalInput * _controlRollFactor;
        float roll = Mathf.Lerp(transform.localRotation.z, rollDueToControl, 0.5f);
        
        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void Shoot(bool shootInput)
    {
        foreach (ParticleSystem laser in _lasers)
        {
            var emmision = laser.emission;
            emmision.enabled = shootInput;
        }
    }
}
