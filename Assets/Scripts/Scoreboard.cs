using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Scoreboard : MonoBehaviour
{
    private int _score = 0;
    private TMP_Text _scoreText;

    private void Start()
    {
        _scoreText = GetComponent<TMP_Text>();
    }

    public void IncreaseScore(int amount)
    {
        _score += amount;
        _scoreText.text = _score.ToString();
    }
}
