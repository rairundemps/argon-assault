using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    [SerializeField] private float _timeTillDestroy = 1.1f;

    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, _timeTillDestroy);
    }
}
