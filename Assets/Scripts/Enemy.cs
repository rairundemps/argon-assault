using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private GameObject _deathVFX;
    [SerializeField] private int _scorePoints = 3;
    [SerializeField] private int _hitPoints = 20;

    private Scoreboard _scoreboard;
    private Transform _parent;

    private void Start()
    {
        _scoreboard = FindObjectOfType<Scoreboard>();
        _parent = GameObject.FindWithTag("SpawnAtRuntime").transform;

        Rigidbody rigidbody = gameObject.AddComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    private void OnParticleCollision(GameObject other)
    {
        _scoreboard.IncreaseScore(_scorePoints);

        _hitPoints--;

        if (_hitPoints < 0)
            _hitPoints = 0;

        if (_hitPoints == 0)
            Die();
    }

    private void Die()
    {
        GameObject currentDeathVFX = Instantiate(_deathVFX, transform.position, Quaternion.identity);
        currentDeathVFX.transform.parent = _parent;
        currentDeathVFX.transform.localScale = transform.localScale;
        Destroy(gameObject);
    }
}
