using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    private void Awake()
    {
        int number = FindObjectsOfType<MusicPlayer>().Length;

        if (number > 1)
            Destroy(gameObject);
        else
            DontDestroyOnLoad(gameObject);
    }
}
